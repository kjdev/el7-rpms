# CentOS 7 - RPM files

## Install

Install with yum.

```
$ sudo cat > /etc/yum.repos.d/kjdev.repo <<EOF
[kjdev]
name=CentOS \$releasever - \$basearch - kjdev
baseurl=https://bitbucket.org/kjdev/el\$releasever-rpms/raw/master/RPMS/
enabled=1
gpgcheck=0
EOF
```

RPM install.

```
$ yum install <PACKAGEs>
```
